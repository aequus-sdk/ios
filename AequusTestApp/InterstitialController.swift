//
//  ViewController.swift
//  ads
//
//  Created by Oleh Veheria on 16.10.2020.
//

import UIKit
import AequusCore
import Combine
import AdSupport
import AppTrackingTransparency

class InterstitialController: UIViewController {
	
    
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    @IBOutlet weak var startButton: SimpleButton!
    @IBOutlet weak var showButton: SimpleButton!
    @IBOutlet weak var destroyButton: SimpleButton!
    
    private var initTimer: Timer?
    private var adsTimer: Timer?
    private var interstitial: AequusInterstitial?
	
	private var cancellableBag: [AnyCancellable] = []
    
    let placementName = "defaultInterstitial"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startButton.isEnabled = true
        showButton.isEnabled = true
        destroyButton.isEnabled = false
        
        destroyButton.isHidden = true

        tabBarItem.accessibilityIdentifier = "InterstitialTab"
        tabBarItem.accessibilityLabel = "InterstitialTab"
        tabBarItem.accessibilityTraits = .tabBar
        
        startButton.accessibilityIdentifier = "StartButton"
        startButton.accessibilityLabel = "StartButton"
        startButton.accessibilityTraits = .button
        
        showButton.accessibilityIdentifier = "ShowButton"
        showButton.accessibilityLabel = "ShowButton"
        showButton.accessibilityTraits = .button
        
        destroyButton.accessibilityIdentifier = "DestroyButton"
        destroyButton.accessibilityLabel = "DestroyButton"
        destroyButton.accessibilityTraits = .button
        
        refreshButton.accessibilityIdentifier = "RefreshButton"
        refreshButton.accessibilityLabel = "RefreshButton"
        refreshButton.accessibilityTraits = .button

		let progressIndicator = UIActivityIndicatorView(style: .medium)
		progressIndicator.color = .systemBlue
		progressIndicator.startAnimating()
		let barButtonItem = UIBarButtonItem(customView: progressIndicator)
		self.navigationItem.rightBarButtonItem = barButtonItem
		
		Aequus.shared.initialisationPublisher
			.receive(on: DispatchQueue.main)
			.sink { initialized in
				if initialized {
					self.navigationItem.rightBarButtonItem = .none
				}
				
			}
			.store(in: &cancellableBag)
		
        self.interstitial = Aequus.shared.createInterstitial(for: self.placementName, viewController: self, delegate: self)
			
		self.interstitial?.firstAdToDisplayDelegate = self
        
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if #available(iOS 14, *) {
			ATTrackingManager
				.requestTrackingAuthorization(
					completionHandler: { status in
						if status == .authorized {
							UIPasteboard.general.string = ASIdentifierManager.shared().advertisingIdentifier.uuidString
						}
						
					})
		} else {
			// Fallback on earlier versions
		}

	}
    
    //MARK: - Actions
    
    @IBAction func startTapped(_ sender: SimpleButton) {
        interstitial?.load()
    }
    
    @IBAction func showTapped(_ sender: SimpleButton) {
        interstitial?.show(from: self)
    }
    
    @IBAction func destroyTapped(_ sender: SimpleButton) {
        interstitial?.destroy()
    }

    @IBAction func refreshTapped(_ sender: Any) {
        interstitial?.destroy()
    }
    
}

extension InterstitialController: AequusInterstitialDelegate, AequusFirstAdToDisplayDelegate {
	func firstAdToDisplayChanged(impressionData: ImpressionData, adType: AdType) {
		print("firstAdToDisplay \(impressionData.impressionId)")
	}
    
    func failToShow(ad: AequusAd, with error: Error) {
		print("#event interstitial fail to show \(error.localizedDescription)")
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            alert.preferredAction = ok
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func didLoad(ad: AequusAd) {
		print("#event interstitial did load")
    }
    
    func failToLoad(ad: AequusAd, with error: Error) {
		print("#event interstitial fail to load \(error.localizedDescription)")
        DispatchQueue.main.async {
            self.startButton.isEnabled = true
            self.showButton.isEnabled = false

            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            alert.preferredAction = ok
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func didShow(ad: AequusAd) {
		print("#event interstitial did show")
    }
	
    func didHide(ad: AequusAd) {
		print("#event interstitial hide")
    }
    
    func didClick(on ad: AequusAd) {
		print("#event interstitial click")
    }
	
	func impression(on ad: AequusAd) {
		print("#event interstitial impression")
	}
    
}

