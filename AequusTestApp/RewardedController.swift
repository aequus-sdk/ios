//
//  RewardedVideoViewController.swift
//  ads
//
//  Created by Oleh Veheria on 16.10.2020.
//

import UIKit
import AequusCore
import Combine

class RewardedController: UIViewController {
    
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    @IBOutlet weak var startButton: SimpleButton!
    @IBOutlet weak var showButton: SimpleButton!
    @IBOutlet weak var destroyButton: SimpleButton!
    
    private var initTimer: Timer?
    private var adsTimer: Timer?
    private var rewarded: AequusRewarded?
	private var cancelable: AnyCancellable?

    private let placementName = "defaultRewardVideo"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startButton.isEnabled = false
        showButton.isEnabled = false
        destroyButton.isEnabled = false
        
        destroyButton.isHidden = true

        tabBarItem.accessibilityIdentifier = "RewardedTab"
        tabBarItem.accessibilityLabel = "RewardedTab"
        tabBarItem.accessibilityTraits = .tabBar
        
        startButton.accessibilityIdentifier = "StartButton"
        startButton.accessibilityLabel = "StartButton"
        startButton.accessibilityTraits = .button
        
        showButton.accessibilityIdentifier = "ShowButton"
        showButton.accessibilityLabel = "ShowButton"
        showButton.accessibilityTraits = .button
        
        destroyButton.accessibilityIdentifier = "DestroyButton"
        destroyButton.accessibilityLabel = "DestroyButton"
        destroyButton.accessibilityTraits = .button
        
        refreshButton.accessibilityIdentifier = "RefreshButton"
        refreshButton.accessibilityLabel = "RefreshButton"
        refreshButton.accessibilityTraits = .button
      
		let progressIndicator = UIActivityIndicatorView(style: .medium)
		progressIndicator.color = .systemBlue
		progressIndicator.startAnimating()
		let barButtonItem = UIBarButtonItem(customView: progressIndicator)
		self.navigationItem.rightBarButtonItem = barButtonItem
		
		cancelable = Aequus.shared.initialisationPublisher
			.receive(on: DispatchQueue.main)
			.sink { initialized in
				if initialized {
					self.navigationItem.rightBarButtonItem = .none
				}
			}
        
        startButton.isEnabled = true
        rewarded = Aequus.shared.createRewarded(for: placementName, viewController: self, delegate: self)
		rewarded?.firstAdToDisplayDelegate = self
        
    }

    //MARK: - Actions
    
    @IBAction func startTapped(_ sender: SimpleButton) {
        rewarded?.load()
    }
    
    @IBAction func showTapped(_ sender: SimpleButton) {
        rewarded?.show(from: self)
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
        rewarded?.destroy()
    }
    
}


extension RewardedController: AequusFirstAdToDisplayDelegate {
	func firstAdToDisplayChanged(impressionData: ImpressionData, adType: AdType) {
		print("firstAdToDisplay \(impressionData.impressionId)")
	}
}

extension RewardedController: AequusRewardedDelegate {
    
    func failToShow(ad: AequusAd, with error: Error) {
		print("#event rewarded fail to show \(error.localizedDescription)")
        DispatchQueue.main.async {
            self.startButton.isEnabled = true
            self.showButton.isEnabled = false

            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            alert.preferredAction = ok
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func userRewarded(ad: AequusAd) {
		print("#event rewarded reward")
    }
    
    func rewardedVideoStarted(ad: AequusAd) {
		print("#event rewarded video started") //can be ignored
    }
    
    func rewardedVideoCompleted(ad: AequusAd) {
		print("#event rewarded video complete") //can be ignored
    }
    
    func didLoad(ad: AequusAd) {
		print("#event rewarded did load")
        DispatchQueue.main.async {
            self.startButton.isEnabled = false
            self.showButton.isEnabled = true
        }
    }
    
    func failToLoad(ad: AequusAd, with error: Error) {
		print("#event rewarded fail to load \(error.localizedDescription)")
        DispatchQueue.main.async {
            self.startButton.isEnabled = true
            self.showButton.isEnabled = false

            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            alert.preferredAction = ok
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func didShow(ad: AequusAd) {
		print("#event rewarded did show")
		DispatchQueue.main.async {
			self.startButton.isEnabled = true
			self.showButton.isEnabled = false
		}
    }  
    
    func didHide(ad: AequusAd) {
		print("#event rewarded hide")
        DispatchQueue.main.async {
            self.startButton.isEnabled = true
            self.showButton.isEnabled = false
        }
    }
    
    func didClick(on ad: AequusAd) {
		print("#event rewarded click")
    }
    
	func impression(on ad: AequusAd) {
		print("#event rewarded impression")
	}

}
