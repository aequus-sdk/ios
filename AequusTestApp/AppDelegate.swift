//
//  AppDelegate.swift
//  AequusTestApp
//
//  Created by Aleksandr on 14.02.2022.
//

import UIKit
import AppTrackingTransparency
import AequusCore
import AdSupport

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 14, *) {
            ATTrackingManager
                .requestTrackingAuthorization(
                    completionHandler: { status in
                        if status == .authorized {
                            UIPasteboard.general.string = ASIdentifierManager.shared().advertisingIdentifier.uuidString
                        }
                    })
        }
        
		#error("use your api key from a Aequus portal")
		var apiKey = "API_KEY"
		#error("use your access key from a Aequus portal")
        var acsKey = "ACCESS_KEY"

        Aequus.shared.userID = "this is my user id"
        Aequus.shared.initialise(
            apiKey: apiKey,
            acsKey: acsKey) { isInitialized, error  in
            print("SDK initialized: \(isInitialized)")
        }
        
        Aequus.shared.ilrdDelegate = self
        
        let p = AequusPrivacy
            .Builder()
            .CCPADoNotSell(true)
            .GDPRConsent(true)
            .underAgeOfConsent(true)
            .build()
        
        Aequus.shared.privacy = p

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate: ILRDDelegate {
    func adDidLoad(impressionData: ImpressionData) {
        print("""
                ----ILRDDelegate----
                    ILRDDelegate didLoad
                    networkName: \(impressionData.networkName)
                -----------
                """)
    }
    
    func adDidShow(impressionData: ImpressionData) {
        print("""
                ----ILRDDelegate----
                    ILRDDelegate didShow
                    networkName: \(impressionData.networkName)
                -----------
                """)
    }
}

