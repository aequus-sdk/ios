//
//  BannerViewController.swift
//  ads
//
//  Created by Oleh Veheria on 16.10.2020.
//

import UIKit
import AequusCore
import Combine

class BannerController: UIViewController {
    
    @IBOutlet weak var adContainerView: UIView!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    @IBOutlet weak var startButton: SimpleButton!
    @IBOutlet weak var destroyButton: SimpleButton!
    
    var banner: AequusBannerAdView?
    
    private var timer: Timer?
	private var cancelable: AnyCancellable?
    
    private let placementName = "defaultBanner"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        destroyButton.isHidden = true
        
        tabBarItem.accessibilityIdentifier = "BannerTab"
        tabBarItem.accessibilityLabel = "BannerTab"
        tabBarItem.accessibilityTraits = .tabBar
        
        startButton.accessibilityIdentifier = "StartButton"
        startButton.accessibilityLabel = "StartButton"
        startButton.accessibilityTraits = .button
        
        refreshButton.accessibilityIdentifier = "RefreshButton"
        refreshButton.accessibilityLabel = "RefreshButton"
        refreshButton.accessibilityTraits = .button
        
        destroyButton.accessibilityIdentifier = "DestroyButton"
        destroyButton.accessibilityLabel = "DestroyButton"
        destroyButton.accessibilityTraits = .button
        
		let progressIndicator = UIActivityIndicatorView(style: .medium)
		progressIndicator.color = .systemBlue
		progressIndicator.startAnimating()
		let barButtonItem = UIBarButtonItem(customView: progressIndicator)
		self.navigationItem.rightBarButtonItem = barButtonItem
		
		cancelable = Aequus.shared.initialisationPublisher
			.receive(on: DispatchQueue.main)
			.sink { initialized in
				if initialized {
					self.navigationItem.rightBarButtonItem = .none
				}
			}
		
        startButton.isEnabled = true
        banner = Aequus.shared.createBanner(for: placementName, viewController: self, size: AequusBannerSize.w320h50, delegate: self)
        
	}
    
    //MARK: - Actions
    
    @IBAction func startTapped(_ sender: SimpleButton) {
        destroyButton.isEnabled = true
        
        if let banner = banner {
            banner.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            self.adContainerView.addSubview(banner)
        }
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
        banner?.destroy()
        adContainerView.subviews.forEach { $0.removeFromSuperview() }
        destroyButton.isEnabled = false
        startButton.isEnabled = true
    }
}

extension BannerController: AequusBannerDelegate {
    func didShow(ad: AequusAd) {
        
    }
    
    func didClick(on ad: AequusAd) {
        
    }
    
    func failToLoad(ad: AequusAd, with error: Error) {
        self.startButton.isEnabled = true

        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(ok)
        alert.preferredAction = ok
        self.present(alert, animated: true, completion: nil)
    }
    
    func failToShow(ad: AequusAd, with error: Error) {
        
    }
    
    func didHide(ad: AequusAd) {
        
    }
    
    func didLoad(ad: AequusAd) {
        
    }

	func impression(on ad: AequusAd) {
		
	}

}
