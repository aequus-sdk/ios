//
//  SimpleButton.swift
//  test-app
//
//  Created by Oleh Veheria on 29.12.2020.
//

import UIKit

class SimpleButton: UIButton {
  
  required init() {
    super.init(frame: .zero)
    layer.cornerRadius = 8
    clipsToBounds = true
  }
    
  required init?(coder aDecoder: NSCoder) {
     super.init(coder: aDecoder)
  }
  
  func setActive() {
    isUserInteractionEnabled = true
    setTitleColor(.blue, for: .normal)
  }
  
  func setInactive() {
    isUserInteractionEnabled = false
    setTitleColor(.gray, for: .normal)
  }
}
